@file:Suppress("UnusedImport")

package com.femastudios.dataflow.android.demo

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.femastudios.android.core.dp
import com.femastudios.android.core.setPadding
import com.femastudios.dataflow.android.declarativeui.add
import com.femastudios.dataflow.android.declarativeui.new
import com.femastudios.dataflow.android.declarativeui.setContent
import com.femastudios.dataflow.android.listen
import com.femastudios.dataflow.android.recyclerView.FieldAdapter
import com.femastudios.dataflow.android.viewState.extensions.setText
import com.femastudios.dataflow.android.viewState.extensions.setTwoWayText
import com.femastudios.dataflow.android.viewState.extensions.setVisible
import com.femastudios.dataflow.async.Attribute
import com.femastudios.dataflow.async.Error
import com.femastudios.dataflow.async.FlowStrategy
import com.femastudios.dataflow.async.extensions.sortedBy
import com.femastudios.dataflow.async.extensions.valueOr
import com.femastudios.dataflow.async.util.async
import com.femastudios.dataflow.extensions.gt
import com.femastudios.dataflow.extensions.length
import com.femastudios.dataflow.util.mutableFieldOf
import java.io.IOException
import kotlin.random.Random


class MainActivity : AppCompatActivity() {

	//The source of truth
	private val searchQuery = mutableFieldOf("")
	//The Wikipedia results for the searchQuery
	private val results = searchQuery
		.async()
		.debounced(250, 1000)
		.transform {
			try {
				WikipediaUtils.search(it)
			} catch (e: IOException) {
				throwError("Error downloading results")
			}
		}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContent.linearLayout(vertical = true) {
			//Create the search bar
			add.editText(searchQuery) {
				hint = "Search on Wikipedia"
			}

			//Handle progress
			add.circleProgressBar {
				setVisible(results.isLoading())
			}
			//Handle retry
			add.linearLayout(vertical = true) {
				setVisible(results.isError())
				add.textError(results)
				add.retryButton("Retry", results)
			}
			//Show results
			add { weight = 1f }.recyclerView(
				results.valueOr(emptyList()),
				{ it.pageId },
				{
					new.text { setPadding(dp(8)) }
				},
				{ v, item -> v.text = item.title }
			) {
				setVisible(results.isLoaded())
			}
		}
	}

	/*override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		val container = LinearLayout(this)
		container.orientation = LinearLayout.VERTICAL

		//Create the search bar
		val searchBar = EditText(this)
		searchBar.setTwoWayText(searchQuery)
		searchBar.hint = "Search on Wikipedia"
		container.addView(searchBar)

		//Handle progress
		val progressBar = ProgressBar(this)
		progressBar.setVisible(results.isLoading())
		container.addView(progressBar)

		//Handle retry
		val errorLayout = LinearLayout(this)
		errorLayout.orientation = LinearLayout.VERTICAL
		errorLayout.setVisible(results.isError())
		val errorText = TextView(this)
		errorText.setText(results.asField().transform {
			(it as? Error)?.displayableMessage ?: "Generic error"
		})
		errorLayout.addView(errorText)
		val retryButton = Button(this)
		retryButton.text = "Retry"
		retryButton.setOnClickListener {
			results.recompute()
		}
		container.addView(errorLayout)

		//Show results
		val resultsView = RecyclerView(this)
		resultsView.layoutManager = LinearLayoutManager(this)
		resultsView.adapter = FieldAdapter.of(
			results.valueOr(emptyList()),
			{ it.pageId },
			{ new.text() },
			{ v, item -> v.text = item.title }
		)
		container.addView(resultsView)

		setContentView(container)
	}*/
}
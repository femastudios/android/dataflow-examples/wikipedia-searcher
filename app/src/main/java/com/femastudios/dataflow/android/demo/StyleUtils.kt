@file:Suppress("UnusedImport")

package com.femastudios.dataflow.android.demo

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.TypedArray
import android.graphics.drawable.Drawable
import android.view.View
import androidx.core.content.res.use
import com.femastudios.dataflow.android.declarativeui.ViewCreator
import com.femastudios.dataflow.async.Attribute
import com.femastudios.dataflow.async.Error


fun ViewCreator.textError(attribute: Attribute<*>) {
	text(attribute.asField().transform {
		when (val error = it as? Error) {
			null -> null
			else -> error.displayableMessage ?: "Generic error"
		}
	})
}

fun ViewCreator.retryButton(text: String, attribute: Attribute<*>) {
	button(text, attribute.asField().transform { data ->
		when (val retry = (data as? Error)?.retry) {
			null -> null
			else -> View.OnClickListener { retry() }
		}
	})
}

fun Context.drawableAttr(attr: Int): Drawable {
	val typedArray = obtainStyledAttributes(intArrayOf(attr))
	val d = typedArray.getDrawable(0)!!
	typedArray.recycle()
	return d
}

/*
fun TextView.setText(text: Attribute<CharSequence?>) {
    setTextViewState(text.toViewState(
        null,
        onLoading = { backgroundColor(Color.GRAY) + text("Loading...") },
        onError = { backgroundColor(Color.RED) + text(it.displayableMessage) },
        onLoaded = { text(it) }
    ))
}
*/

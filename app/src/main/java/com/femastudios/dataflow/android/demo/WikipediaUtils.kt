package com.femastudios.dataflow.android.demo

import com.femastudios.dataflow.util.mutableFieldOf
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONObject
import java.io.IOException


object WikipediaUtils {

	class WikipediaItem(
		val pageId: Long,
		val title: String
	) {
		val likeCount = mutableFieldOf(0)
	}

	private val cache = mutableMapOf<Long, WikipediaItem>()

	/**
	 * Downloads a list of [WikipediaItem] for the given [query]
	 */
	@Throws(IOException::class)
	fun search(query: String): List<WikipediaItem> {
		if (query.isBlank()) return emptyList()

		//Thread.sleep(1000) //Simulate a longer IO delay

		OkHttpClient().newCall(
			Request.Builder()
				.url(
					HttpUrl.Builder()
						.scheme("https")
						.host("it.wikipedia.org")
						.addPathSegments("w/api.php")
						.addQueryParameter("action", "query")
						.addQueryParameter("list", "search")
						.addQueryParameter("srsearch", query)
						.addQueryParameter("utf8", "")
						.addQueryParameter("srlimit", "5")
						.addQueryParameter("format", "json")
						.build()
				)
				.build()
		).execute().use { r ->
			val body = r.body?.string() ?: throw IOException()
			val jResults = JSONObject(body).getJSONObject("query").getJSONArray("search")
			return List(jResults.length()) {
				val jResult = jResults[it] as JSONObject
				val pageId = jResult.getLong("pageid")
				synchronized(cache) {
					cache.getOrPut(pageId) {
						WikipediaItem(pageId, jResult.getString("title"))
					}
				}
			}
		}
	}
}
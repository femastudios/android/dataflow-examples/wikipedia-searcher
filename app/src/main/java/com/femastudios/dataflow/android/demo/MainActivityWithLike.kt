@file:Suppress("UnusedImport")
@file:SuppressLint("ViewConstructor")

package com.femastudios.dataflow.android.demo

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.femastudios.android.core.dp
import com.femastudios.android.core.setPadding
import com.femastudios.android.core.setPaddingHorizontal
import com.femastudios.android.core.setPaddingVertical
import com.femastudios.dataflow.Field
import com.femastudios.dataflow.android.declarativeui.add
import com.femastudios.dataflow.android.declarativeui.new
import com.femastudios.dataflow.android.declarativeui.setContent
import com.femastudios.dataflow.android.viewState.extensions.setBackground
import com.femastudios.dataflow.android.viewState.extensions.setVisible
import com.femastudios.dataflow.async.FlowStrategy
import com.femastudios.dataflow.async.extensions.*
import com.femastudios.dataflow.async.util.async
import com.femastudios.dataflow.extensions.increment
import com.femastudios.dataflow.extensions.thenOrNull
import com.femastudios.dataflow.util.mutableFieldOf
import java.io.IOException


class MainActivityWithLike : AppCompatActivity() {

	//The source of truth
	private val searchQuery = mutableFieldOf("")
	//The Wikipedia results for the searchQuery
	private val results = searchQuery
		.async()
		.debounced(250, 1000)
		.transform {
			try {
				WikipediaUtils.search(it)
			} catch (e: IOException) {
				throwError("Error downloading results")
			}
		}

	class ItemView(context: Context, item: Field<WikipediaUtils.WikipediaItem?>) : LinearLayout(context) {
		init {
			gravity = Gravity.CENTER_VERTICAL
			setPadding(dp(8))
			add { weight = 1f }.text(item.transform { it?.title })
			add.imageButton(R.drawable.ic_favorite_black_24dp, item.transform { i ->
				if (i != null) {
					OnClickListener { i.likeCount.increment() }
				} else null
			}) {
				background = context.drawableAttr(android.R.attr.selectableItemBackgroundBorderless)
			}
			add.text(item.thenOrNull { it.likeCount }.transform { "$it likes" }) {
				minWidth = dp(64)
			}
		}
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContent.linearLayout(vertical = true) {
			//Create the search bar
			add.editText(searchQuery) {
				hint = "Search on Wikipedia"
			}
			//Handle progress
			add.circleProgressBar {
				setVisible(results.isLoading())
			}
			//Handle retry
			add.linearLayout(vertical = true) {
				setVisible(results.isError())
				add.textError(results)
				add.retryButton("Retry", results)
			}
			//Show results
			add { weight = 1f }.recyclerView(
				results.sortedByDescendingA { it.likeCount.async() }.valueOr(emptyList()),
				{ it.pageId },
				{ ctx, item -> ItemView(ctx, item) }
			) {
				setVisible(results.isLoaded())
			}
		}
	}
}